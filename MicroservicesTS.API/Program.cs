﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MicroservicesTS.API.Infrastructure.Middlewares;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
//Serilog-StartTS 
using Serilog;
//Serilog-EndTS

namespace MicroservicesTS.API
{
    public class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;
        public static readonly string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        public static int Main(string[] args)
        {
            var configuration = GetConfiguration();

            //Serilog-StartTS   
            Log.Logger = CreateSerilogLogger(configuration);
            //Serilog-EndTS  

            try
            {

                //Serilog-StartTS  
                Log.Information("Configuring web host ({ApplicationContext})...", AppName);
                //Serilog-EndTS 

                var host = BuildWebHost(configuration, args);

                //Serilog-StartTS 
                Log.Information("StartTSing web host ({ApplicationContext})...", AppName);
                //Serilog-EndTS  
                host.Run();

                return 0;
            }
            catch (Exception ex)
            {
                //Serilog-StartTS  
                Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", AppName);
                //Serilog-EndTS  
                return 1;
            }
            //Serilog-StartTS  
            finally
            {
                Log.CloseAndFlush();
            }
            //Serilog-EndTS  
        }

        private static IWebHost BuildWebHost(IConfiguration configuration, string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .CaptureStartupErrors(false)
                .UseFailing(options =>
                    options.ConfigPath = "/Failing")
                .UseStartup<Startup>()
               // .UseApplicationInsights()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseConfiguration(configuration)
                //Serilog-StartTS
                .UseSerilog()
                //Serilog-EndTS 
                .Build();

        //Serilog-StartTS  
        private static Serilog.ILogger CreateSerilogLogger(IConfiguration configuration)
        {
            //TODO: finish here the connection string
            var seqServerUrl = configuration["Serilog:SeqServerUrl"];

            return new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .Enrich.WithProperty("ApplicationContext", AppName)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.Seq(string.IsNullOrWhiteSpace(seqServerUrl) ? "http://seq" : seqServerUrl)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }
        //Serilog-EndTS  

        private static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                //.AddEnvironmentVariables();

            var config = builder.Build();

            //AzureKeyVault-StartTS
            if (config.GetValue<bool>("UseVault", false))
            {
                builder.AddAzureKeyVault(
                    $"https://{config["Vault:Name"]}.vault.azure.net/",
                    config["Vault:ClientId"],
                    config["Vault:ClientSecret"]);
            }
            //AzureKeyVault-EndTS  

            return builder.Build();
        }
    }
}

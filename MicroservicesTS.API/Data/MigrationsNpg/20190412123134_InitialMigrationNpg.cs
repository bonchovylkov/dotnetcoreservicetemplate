﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace MicroservicesTS.API.MigrationsNpg
{
    public partial class InitialMigrationNpg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: ("MicroservicesTS_entities").ToLower(),
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: true),
                    is_deleted = table.Column<bool>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey(("pk_MicroservicesTS_entities").ToLower(), x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: ("ix_MicroservicesTS_entities_is_deleted").ToLower(),
                table: ("MicroservicesTS_entities").ToLower(),
                column: "is_deleted");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: ("MicroservicesTS_entities").ToLower());
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MicroservicesTS.API.MigrationsMySql
{
    public partial class InitialMig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MicroservicesTSEntities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<short>(type: "bit", nullable: false),
                    DeletedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MicroservicesTSEntities", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MicroservicesTSEntities_IsDeleted",
                table: "MicroservicesTSEntities",
                column: "IsDeleted");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MicroservicesTSEntities");
        }
    }
}

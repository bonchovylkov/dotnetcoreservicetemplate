﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MicroservicesTS.API.Data
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<MicroservicesTSContext>
    {
        public MicroservicesTSContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            var builder = new DbContextOptionsBuilder<MicroservicesTSContext>();

            var connectionString = configuration.GetConnectionString("DefaultConnection");

            //SQLServer-StartTS
            builder.UseSqlServer(connectionString);
            //SQLServer-EndTS

            //PostgreSQL-StartTS
            builder.UseNpgsql(connectionString);
            //PostgreSQL-EndTS

            //MySQL-StartTS
            builder.UseMySQL(connectionString);
            //MySQL-EndTS

            // Stop client query evaluation
            builder.ConfigureWarnings(w => w.Throw(RelationalEventId.QueryClientEvaluationWarning));

            return new MicroservicesTSContext(builder.Options);
        }
    }
}

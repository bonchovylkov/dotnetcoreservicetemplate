﻿namespace MicroservicesTS.API.Data.Common.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public abstract class BaseDeletableModel<TKey> : BaseModel<TKey>, IDeletableEntity
    {
        //MySQL-StartTS
        [Column("IsDeleted", TypeName = "bit")]
        //MySQL-EndTS
        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }
    }
}

﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MicroservicesTS.API.Data.Common.Repositories;
using MicroservicesTS.API.Data.Models;
using MicroservicesTS.API.Infrastructure.Mapping;
using MicroservicesTS.API.Infrastructure.ViewModels.MicroservicesTS;
using Microsoft.AspNetCore.Mvc;

//RabbitMQ-StartTS
using MicroservicesTS.API.Infrastructure.EventBus;
//RabbitMQ-EndTS

//IdentityServer-StartTS
using Microsoft.AspNetCore.Authorization;
//IdentityServer-EndTS

namespace MicroservicesTS.API.Controllers
{
    [Route("api/[controller]")]
    //IdentityServer-StartTS
    [Authorize]
    //IdentityServer-EndTS
    [ApiController]
    public class MicroservicesTSController : ControllerBase
    {
        private IDeletableEntityRepository<MicroservicesTSEntity> repo;
        //RabbitMQ-StartTS
        private IMessageBus messageBus;
        //RabbitMQ-EndTS

        public MicroservicesTSController(IDeletableEntityRepository<MicroservicesTSEntity> service
            //RabbitMQ-StartTS
            , IMessageBus messageBus
            //RabbitMQ-EndTS
            )
        {
            this.repo = service;

            //RabbitMQ-StartTS
            this.messageBus = messageBus;
            //RabbitMQ-EndTS
        }

        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            var item = await this.repo.GetByIdAsync(id);
            return Ok(item);
        }

        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll(int skip = 0, int take = 100)
        {
            var items = this.repo.All()
                .OrderBy(s => s.Id)
                .Skip(skip)
                .Take(take)
                .To<MicroservicesTSViewModel>();

            return Ok(items);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]MicroservicesTSViewModel newItem)
        {

            var dbItem = Mapper.Map<MicroservicesTSEntity>(newItem);
            this.repo.Add(dbItem);
            await this.repo.SaveChangesAsync();


            //RabbitMQ-StartTS
            this.messageBus.Publish(
             string.Format("New object of type: MicroservicesTSViewModel in Microservice: MicroservicesTS, with id {0}", dbItem.Id) , "EventBusReplaceTopics");
            //RabbitMQ-EndTS



            return Ok(new { id = dbItem.Id });
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]MicroservicesTSViewModel newItem)
        {
            var dbItem = await this.repo.GetByIdAsync(newItem.Id);
            //TODO: add properties which want to update
            this.repo.Update(dbItem);
            await this.repo.SaveChangesAsync();

            //RabbitMQ-StartTS
            this.messageBus.Publish( 
             string.Format("Object was updated in microservice: MicroservicesTS, with id {0}", newItem.Id) , "EventBusReplaceTopics");
            //RabbitMQ-EndTS

            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await this.repo.GetByIdAsync(id);
            this.repo.Delete(item);
            await this.repo.SaveChangesAsync();

            //RabbitMQ-StartTS
            this.messageBus.Publish(
             string.Format("Object was deleted in microservice: MicroservicesTS, with id {0}", id) , "EventBusReplaceTopics");
            //RabbitMQ-EndTS
            return Ok();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroservicesTS.API
{
    public class MicroservicesTSSettings
    {
        public string ConnectionString { get; set; }
        public string RabbitMQConnectionString { get; set; }
    }
}

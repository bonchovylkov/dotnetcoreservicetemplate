﻿using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;

namespace MicroservicesTS.API.Auth.Server
{
    public class IdentitySecurityScheme:SecurityScheme
    {
        public IdentitySecurityScheme()
        {
            Type = "IdentitySecurityScheme";
            Description = "Security definition that provides to the user of Swagger a mechanism to obtain a token from the identity service that secures the api";
            //TODO:think about it
            Extensions.Add("authorizationUrl", "http://localhost:5000/Auth/Client/popup.html");
            Extensions.Add("flow", "implicit");
            Extensions.Add("scopes", new List<string>
            {
                ("MicroservicesTS").ToLower()
            });
        }
    }
}

﻿namespace MicroservicesTS.API.Services.Identity
{
    public interface IIdentityService
    {
        string GetUserIdentity();
    }
}

﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.Options;
using MicroservicesTS.API.Infrastructure.Filters;
using System.Collections.Generic;
using System.Reflection;

//RabbitMQ-StartTS
using MicroservicesTS.API.Infrastructure.EventBus;
//RabbitMQ-EndTS

//Redis-StartTS
using StackExchange.Redis;
//Redis-EndTS
using Microsoft.AspNetCore.Http;

//Database-StartTS
using MicroservicesTS.API.Data;
using MicroservicesTS.API.Data.Common.Repositories;
using Microsoft.EntityFrameworkCore;
//Database-EndTS

//Automapper-StartTS
using MicroservicesTS.API.Infrastructure.Mapping;
//Automapper-EndTS 

//IdentityServer-StartTS
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using MicroservicesTS.API.Services.Identity;
//IdentityServer-EndTS 

namespace MicroservicesTS.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                options.Filters.Add(typeof(ValidateModelStateFilter));

            })
             .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
             .AddControllersAsServices();


            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Info
                {
                    Title = "My MicroservicesTS API",
                    Version = "v1",
                    Description = "The MicroservicesTS Service HTTP API",
                    TermsOfService = "Terms Of Service"
                });

                //IdentityServer-StartTS
                options.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    //AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    //TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    AuthorizationUrl = "http://localhost:5000/connect/authorize",
                    TokenUrl = $"http://localhost:5000/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { ("MicroservicesTS").ToLower(), "MicroservicesTS API" }
                    }
                });

                options.OperationFilter<AuthorizeCheckOperationFilter>();

                //IdentityServer-EndTS 
            });

            //IdentityServer-StartTS
            ConfigureAuthService(services);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IIdentityService, IdentityService>();
            //IdentityServer-EndTS 

            //Database-StartTS

            var connString = this.Configuration.GetConnectionString("DefaultConnection");

            //SQLServer-StartTS
            var hostname = Environment.GetEnvironmentVariable("SQLSERVER_HOST");
            var password = Environment.GetEnvironmentVariable("SA_PASSWORD");
            if (hostname != null && password != null)
            {
                connString = $"Data Source={hostname};Initial Catalog=MicroservicesTSMicroServ;User ID=sa;Password={password};";
            }
            //SQLServer-EndTS

            //PostgreSQL-StartTS
            var postgresPassword = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD");
            if (postgresPassword != null)
            {
                connString = $"User ID=postgres;Password={postgresPassword};Server=postgresdb;Port=5432;Database=MicroservicesTSMicroServ;Integrated Security=true;Pooling=true;";

            }
            //PostgreSQL-EndTS

            //MySQL-StartTS
            var mysqlPassword = Environment.GetEnvironmentVariable("MYSQL_ROOT_PASSWORD");
            if (mysqlPassword != null)
            {
                connString = $"Server=mysqldb;Database=ServiceMySqlMicroServ;userid=root;password={mysqlPassword};";

            }
            //MySQL-EndTS



            services.AddDbContext<MicroservicesTSContext>(options =>
            {
                //SQLServer-StartTS
                options.UseSqlServer(connString,
                                     sqlOpt =>
                                     {
                                         sqlOpt.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                                     });
                //SQLServer-EndTS

                //PostgreSQL-StartTS
                options.UseNpgsql(connString, sqlOptions =>
                {
                    //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
                    sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
                });
                //PostgreSQL-EndTS

                //MySQL-StartTS
                options.UseMySQL(connString);
                //MySQL-EndTS

            });
          

            // Data repositories
            services.AddScoped(typeof(IDeletableEntityRepository<>), typeof(EfDeletableEntityRepository<>));
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            //Database-EndTS

            //RabbitMQ-StartTS
            services.AddSingleton<IMessageBus, MessageBus>();
            services.AddTransient<IMicroservicesTSHandler, MicroservicesTSHandler>();
            //RabbitMQ-EndTS

            services.Configure<MicroservicesTSSettings>(Configuration);

            //Redis-StartTS
            //By connecting here we are making sure that our service
            //cannot start until redis is ready. This might slow down startup,
            //but given that there is a delay on resolving the ip address
            //and then creating the connection it seems reasonable to move
            //that cost to startup instead of having the first request pay the
            //penalty.
            ////TODO: finish here the connection string 
            services.AddSingleton<ConnectionMultiplexer>(sp =>
            {
                var settings = sp.GetRequiredService<IOptions<MicroservicesTSSettings>>().Value;
                var configuration = ConfigurationOptions.Parse(settings.ConnectionString, true);

                configuration.ResolveDns = true;

                return ConnectionMultiplexer.Connect(configuration);
            });
            //Redis-EndTS


            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //Cors-StartTS
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .SetIsOriginAllowed((host) => true)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
            //Cors-EndTS
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //Automapper-StartTS
            AutoMapperConfig.RegisterMappings(typeof(MicroservicesTS.API.Infrastructure.ViewModels.BaseViewModel).GetTypeInfo().Assembly);
            //Automapper-EndTS

            //Database-StartTS
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetRequiredService<MicroservicesTSContext>();

                if (env.IsDevelopment())
                {
                    dbContext.Database.Migrate();
                }

            }
            //Database-EndTS

            //RabbitMQ-StartTS
            var bus = app.ApplicationServices.GetRequiredService<IMessageBus>();
            var handler = app.ApplicationServices.GetRequiredService<IMicroservicesTSHandler>();
            //TODO add some other subscriptions
            bus.Subscribe<string>("MicroservicesTSId", message => handler.Handle(message), x => x.WithTopic("#.MicroservicesTSTopic.#"));
            //RabbitMQ-EndTS

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                app.UsePathBase(pathBase);
            }

            //IdentityServer-StartTS
            app.UseAuthentication();
            //IdentityServer-EndTS

            app.UseMvcWithDefaultRoute();

            //// Enable middleware to serve generated Swagger as a JSON endpoint.
            //app.UseSwagger();

            //// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My MicroservicesTS.API API V1");
            //});

            app.UseSwagger()
            .UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"{ (!string.IsNullOrEmpty(pathBase) ? pathBase : string.Empty) }/swagger/v1/swagger.json", "MicroservicesTS.API V1");
                //IdentityServer-StartTS
                c.OAuthClientId(("MicroservicesTSswaggerui").ToLower());
                c.OAuthAppName("MicroservicesTS Swagger UI");
                //IdentityServer-EndTS
            });

            app.UseStaticFiles();

            //Cors-StartTS
            app.UseCors("CorsPolicy");
            //Cors-EndTS

        }

        //IdentityServer-StartTS
        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            //TODO:think about it
            var identityUrl = "http://localhost:5000";//Configuration.GetValue<string>("IdentityUrl");

            services
                 .AddAuthorization()
                .AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.Authority = identityUrl;
                options.RequireHttpsMetadata = false;
                options.Audience = ("MicroservicesTS").ToLower();
            });
        }
        //IdentityServer-EndTS 

    }
}

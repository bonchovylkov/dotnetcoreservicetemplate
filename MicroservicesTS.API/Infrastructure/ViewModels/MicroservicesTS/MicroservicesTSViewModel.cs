﻿
using MicroservicesTS.API.Data.Models;
using MicroservicesTS.API.Infrastructure.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroservicesTS.API.Infrastructure.ViewModels.MicroservicesTS
{
    public class MicroservicesTSViewModel : BaseDeletableViewModel, IMapFrom<MicroservicesTSEntity>
    {
    }
}

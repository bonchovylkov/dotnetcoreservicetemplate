﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroservicesTS.API.Infrastructure.EventBus
{
    public interface IMessageBus
     {
        void Publish<T>(T message) where T : class;
        void Subscribe<T>(string MicroservicesTSId, Action<T> action) where T : class;
        void Publish<T>(T message,string topic) where T : class;
        void Subscribe<T>(string MicroservicesTSId, Action<T> action, Action<EasyNetQ.FluentConfiguration.ISubscriptionConfiguration> topicAction) where T : class;
        void Send<T>(string queue, T message) where T : class;
        void Receive<T>(string queue, Action<T> action) where T : class;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroservicesTS.API.Infrastructure.EventBus
{
    public class MicroservicesTSHandler : IMicroservicesTSHandler
    {
        public void Handle(string message)
        {
            //TODO: handle your event like you wish
            Console.WriteLine("MicroservicesTSHandler:" + message);
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("MicroservicesTSIntegrationEvents.txt", true))
            {
                file.WriteLine(DateTime.Now + ":" + message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroservicesTS.API.Infrastructure.EventBus
{
    public interface IMicroservicesTSHandler
    {

        void Handle(string message);
    }
}

﻿using EasyNetQ;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroservicesTS.API.Infrastructure.EventBus
{
    public class MessageBus  : IMessageBus
    {
        private IBus bus;
        MicroservicesTSSettings settings;
        public MessageBus(IOptions<MicroservicesTSSettings> options)
        {
            this.settings = options.Value;
            var connectionString = settings.RabbitMQConnectionString;
            bus = RabbitHutch.CreateBus(string.Format("host={0}", connectionString));
        }

        public void Publish<T>(T message) where T : class
        {
            this.bus.Publish<T>(message);
        }

        public void Publish<T>(T message, string topic) where T : class
        {
            this.bus.Publish<T>(message,topic);
        }

        public void Subscribe<T>(string MicroservicesTSId, Action<T> action) where T: class
        {
            this.bus.Subscribe<T>(MicroservicesTSId, action);
        }

        public void Subscribe<T>(string MicroservicesTSId, Action<T> action, 
            Action<EasyNetQ.FluentConfiguration.ISubscriptionConfiguration> topicAction) where T: class
        {
            this.bus.Subscribe<T>(MicroservicesTSId, action, topicAction);
        }

        public void Receive<T>(string queue, Action<T> action) where T : class
        {
            this.bus.Receive<T>(queue, action);
        }

        public void Send<T>(string queue, T message) where T : class
        {
            this.bus.Send<T>(queue, message);
        }
    }
}

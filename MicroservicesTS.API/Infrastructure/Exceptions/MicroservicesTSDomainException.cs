﻿using System;

namespace MicroservicesTS.API.Infrastructure.Exceptions
{
    /// <summary>
    /// Exception type for app exceptions
    /// </summary>
    public class MicroservicesTSDomainException : Exception
    {
        public MicroservicesTSDomainException()
        { }

        public MicroservicesTSDomainException(string message)
            : base(message)
        { }

        public MicroservicesTSDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
